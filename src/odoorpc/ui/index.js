import { Addons, Menus, Action } from './action'

import { TreeView } from './views/treeview'
import { KanbanView } from './views/kanbanview'
import { FormView } from './views/formview'
import { WizardView } from './views/wizardview'

import { X2mTree } from './views/x2mtree'
import { X2mForm } from './views/x2mform'

import { Field, Relation } from './views/relation'

export default {
  Addons,
  Menus,
  Action,

  TreeView,
  KanbanView,
  FormView,
  WizardView,

  X2mTree,
  X2mForm,

  Field,
  Relation
}
