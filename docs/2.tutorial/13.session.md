### session

#### 修改 odoorpc/web.js 文件 增加 Session 部分

```
import { JsonRequest } from './request'
JsonRequest._session_info = undefined

class Session extends JsonRequest {
  constructor(payload) {
    super(payload)
  }

  static async authenticate({ db, login, password }) {
    const url = '/web/session/authenticate'
    const payload = { db, login, password }
    const info = await this.json_call(url, payload)
    this._session_info = info
    return info
  }
}

// ...

export const web = Home

web.webclient = Webclient
web.database = Database
web.session = Session


```

#### Session 有关的其他接口函数. 备用

```
// ...

class Session extends JsonRequest {
  // ...

  static async get_session_info() {
    const url = '/web/session/get_session_info'
    const info = await this.json_call(url, {})
    this._session_info = info
    return info
  }

  async check() {
    const url = '/web/session/check'
    return await this.json_call(url, {})
  }

  static async destroy() {
    const url = '/web/session/destroy'
    await this.json_call(url, {})
    this._session_info = undefined
    return true
  }

  static async get_lang_list() {
    const url = '/web/session/get_lang_list'
    return await this.json_call(url, {})
  }

  static async modules() {
    const url = '/web/session/modules'
    return await this.json_call(url, {})
  }

  static async change_password({ fields }) {
    // fields 的格式
    // const fields = [
    //   { name: 'old_pwd', value: '123456' },
    //   { name: 'new_password', value: '123456' },
    //   { name: 'confirm_pwd', value: '123456' }
    // ]

    const url = '/web/session/change_password'
    return await this.json_call(url, { fields })
  }
}



```

#### 修改 odoorpc_test/testcase/addons/base.js 文件

```
import rpc from '@/odoorpc'

export default class BaseTestCase {
  constructor({ login_info }) {
    this.login_info = login_info
  }

  async version_info() {
    const res = await rpc.web.webclient.version_info()
    console.log('test get_version_info:', res)
  }

  async login() {
    const { db, login, password } = this.login_info
    return await rpc.web.session.authenticate({ db, login, password })
  }
}

```

#### 新增文件 odoorpc_test/testcase/addons/session.js 文件

```
import BaseTestCase from './base'

export default class SessionTestCase extends BaseTestCase {
  async test() {
    await this.authenticate()
  }

  async authenticate() {
    const { db, login, password } = this.login_info
    const res = await rpc.web.session.authenticate({ db, login, password })
    console.log('test authenticate ', res)
    return res
  }
}


```

#### 修改 odoorpc/test_rpc.js 文件

```
// ...

import Test from './test'

const master_pwd = 'admin'
const login_info = { db: 'test_db', login: 'admin', password: '123456' }

const config = { master_pwd, login_info }
const test = new Test(config)

export const test_rpc = async () => {
  await test.base.version_info()
  await test.session.test()
}


```

### 几个接口的使用场景

1. authenticate 接口, 返回 session_info
2. 后续其他接口会用到 session_info 中的东西, 因此暂存
3. check 接口, 检查服务端 session 是否超期
4. get_session_info 接口, 返回 session_info
5. 跳转新页面时, 应调用 check 接口, 判断服务端是否超期
6. 页面刷新时, 应调用 get_session_info, 获得 session_info

### 关于 cookie 和 seesion_id

1. 认证成功后, 在 respond 的 headers 中携带 session_id
2. session_id 自动存储在 cookie 中
3. 后续调用接口自动携带 cookie
4. 如果是非 web 方式, 如微信小程序或者测试脚本, 调用接口, 则前端没有 cookie
5. 非 web 方式下, 需要额外设置 session_id

### 非 web 方式下, 调用接口时的 seesion_id 设置

#### 修改 odoorpc/request.js 文件

```
class Proxy0 {
  constructor(payload) {
    const { baseURL, timeout = 500000 } = payload
    this._timeout = timeout
    this._baseURL = baseURL
  }
}

Proxy0._sid = undefined

class ProxyJSON extends Proxy0 {
  constructor(payload) {
    const { baseURL, timeout } = payload
    super({ baseURL, timeout })
    this._service = this._get_service()
  }

  _get_service() {
    const service = axios.create({
      baseURL: this._baseURL,
      timeout: this._timeout
    })

    const session_id = this.constructor._sid

    service.interceptors.request.use(
      config => {
        if (session_id) config.headers['X-Openerp-Session-Id'] = session_id
        return config
      },
      error => {
        return Promise.reject(error)
      }
    )

    service.interceptors.response.use(
      response => {
        const url = response.config.url
        const url_auth = '/web/session/authenticate'
        const url_info = '/web/session/get_session_info'

        if (url === url_auth || url === url_info) {
          // if run test or run from wx miniprograme, not cookie,
          // so wo set sid to call odoo

          const headers = response.headers
          const cookie = headers['set-cookie']
          if (cookie) {
            const cookie2 = cookie[0]
            const session_id = cookie2.slice(11, 51)
            Proxy0._sid = session_id
          }
        }

        const res = response.data
        // console.log(response)
        return res
      },
      error => {
        return Promise.reject(error)
      }
    )

    return service
  }

  // ...
}

export class JsonRequest {
  // ...
}

```
