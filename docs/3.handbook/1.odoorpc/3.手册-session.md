## session

### 以下接口返回 session 信息

1. api.web.session.authenticate
2. api.web.session.get_session_info

### session_id

1. 在响应头中, 有 cookie. cookie 中包含 session_id
2. 后续, 调用其他接口时, 需要携带 session_id, 验证授权
3. odoo 服务端验证 session_id, 三种方式:
   a. cookie 中有 session_id  
   b. 请求头文件中有 session_id  
   c. 请求参数中有 session_id
4. 浏览器中发送请求, 自动携带 cookie
5. 非浏览器方式发送请求, 如 微信小程序, 没有 cookie.  
   此时, 需要在授权响应后, 从响应头中获取 session_id, 暂存.  
   并在后续的请求头文件中或请求参数中, 携带 session_id.  
   odoorpc 自动处理 session_id

### session 信息的说明

```
{
  uid: 2,  // user id, integer
  is_system: true,   // 属于权限组  base.group_system
  is_admin: true,   // 属于权限组  base.group_erp_manager

  // context 信息, 再次调用其他接口时, 需要 context
  user_context: { lang: 'zh_CN', tz: 'Asia/Shanghai', uid: 2 },

  // 数据库 database name
  db: 'your_database_name',

  // odoo 版本信息
  server_version: '14.0-20210819',
  server_version_info: [14, 0, 0, 'final', 0, ''],

  name: 'Administrator', // user 的 name
  username: 'your_user_name', // user 的 login
  partner_display_name: 'Administrator', // user 的 display_name
  company_id: 1, // user 的公司 id
  partner_id: 3, // user 的 partner id

  // 用户管理的公司
  user_companies: {
    current_company: [1, 'My Company'],
    allowed_companies: [[1, 'My Company']]
  },

  'web.base.url': 'http://192.168.56.108:8069',
  active_ids_limit: 20000,
  max_file_upload_size: 134217728,

  currencies: {
    '1': { symbol: '€', position: 'after', digits: [69, 2] },
    '2': { symbol: '$', position: 'before', digits: [69, 2] },
    '7': { symbol: '¥', position: 'before', digits: [69, 2] }
  },
  show_effect: 'True',
  display_switch_company_menu: false,
  cache_hashes: {
    load_menus:
      'bee8b9a8892859fadee87b110a9176ae3ee5bf4cfb2048a32d1842527bcf349b',
    qweb: '8f27eaf11534666559d0961f0f0249fbd44fb60a89097b218587223f65270208',
    translations: 'e185b362dbe7b9cb231f6ba4248c8c8247e34dd9'
  },
  user_id: [2],
  max_time_between_keys_in_ms: 55,
  web_tours: [],
  notification_type: 'email',
  odoobot_initialized: true
}


```

### context

##### 调用

1. 在登录授权后, 返回 session 信息
2. 在 session 信息中包含 user_context
3. 后续接口请求中, 多数情况下需要 参数 context
4. odoorpc 模块, 在登录授权后, 自动存储 session 信息.
5. odoorpc 模块, 提供 同步接口, 获取 context

```
import api from '@/odoorpc'
const test_context = async ()=>{
    const context = api.web.session.context
}
```

##### 返回结果

```
{
    lang: "zh_CN",
    tz: "Asia/Shanghai",
    uid: 2,
    allowed_company_ids: [1]
}

```

### 多公司

1. context 中有参数 allowed_company_ids
2. 作用为, 在多公司管理时, 对数据进行过滤
3. 若数据只允许一个公司时, allowed_company_ids 的第一个值有效.
4. 前端 cookie 中有 变量 cids. 暂存 多公司管理的设置
5. context 中的参数 allowed_company_ids, 取自 cookie 中的 cids
   若 cookie 中的 cids 为空, allowed_company_ids 的默认值取自 session 信息中的 user_companies.current_company
6. odoorpc 提供几个接口用于管理 allowed_company_ids.
   api.web.session.allowed_company_ids
   api.web.session.allowed_companies_for_selection
   api.web.session.set_first_allowed_company(cid)
   api.web.session.change_allowed_company(cid, checked)

```
import api from '@/odoorpc'
const test_allowed_company_ids = async ()=>{
    // 读取. 返回 Array
    const cids = api.web.session.allowed_company_ids
    // 设置. 参数为 Array
    api.web.session.allowed_company_ids = [cids]
    // 只读. 当前用户有权限进行管理的公司 ids
    const cids2 = api.web.session.allowed_companies_for_selection
    // 设置 公司cid, 为允许.
    change_allowed_company(cid, true)
    // 设置 公司cid, 为不允许.
    change_allowed_company(cid, false)
    // 设置 公司cid, 为允许. 且排在第一个位置
    set_first_allowed_company(cid)
}

```
